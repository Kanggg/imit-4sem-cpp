#include <iostream>
#include <string>

#include "BSTDictionary.h"

// copy

BSTDictionary::Node *BSTDictionary::copy(const BSTDictionary::Node *src) {
  if (src == nullptr) {
    return nullptr;
  }
  return new Node(
      src->word, src->count,
      copy(src->left),
      copy(src->right)
  );
}

BSTDictionary::BSTDictionary(const BSTDictionary &src) : root(nullptr) {
  root = copy(src.root);
}

BSTDictionary::BSTDictionary(BSTDictionary &&src) noexcept: root(src.root) {
  src.root = nullptr;
}

BSTDictionary &BSTDictionary::operator=(const BSTDictionary &src) {
  if (&src != this) {
    clear();
    root = copy(src.root);
  }
  return *this;
}

BSTDictionary &BSTDictionary::operator=(BSTDictionary &&src) noexcept {
  clear();
  root = src.root;
  src.root = nullptr;
  return *this;
}

// get element's count

unsigned BSTDictionary::find(
    const BSTDictionary::Node *root,
    const std::string &word
) {
  if (root == nullptr) {
    return 0;
  }

  int res = compare_words(word, root->word);
  if (res == 0) {
    return root->count;
  } else if (res < 0) {
    return find(root->left, word);
  } else {
    return find(root->right, word);
  }
}

// insert element

void BSTDictionary::insert(
    BSTDictionary::Node *&root,
    const std::string &word
) {
  if (root == nullptr) {
    root = new Node(word);
    return;
  }

  int res = compare_words(word, root->word);
  if (res == 0) {
    root->count++;
  } else if (res < 0) {
    insert(root->left, word);
  } else {
    insert(root->right, word);
  }
}

// remove element

void BSTDictionary::remove_two(
    BSTDictionary::Node *&root,
    BSTDictionary::Node *&left_child
) {
  if (left_child->right) {
    remove_two(root, left_child->right);
  } else {
    root->count = left_child->count;
    root->word = left_child->word;

    root = left_child;
    left_child = left_child->left;
  }
}

void BSTDictionary::remove(
    BSTDictionary::Node *&root,
    const std::string &word
) {
  if (root == nullptr) {
    return;
  }

  int res = compare_words(word, root->word);
  if (res < 0) {
    remove(root->left, word);
  } else if (res > 0) {
    remove(root->right, word);
  } else {
    if (root->count > 1) {
      root->count--;
    } else {
      BSTDictionary::Node *temp = root;
      if (root->left == nullptr) {
        // only right child
        root = root->right;
      } else if (root->right == nullptr) {
        // only left child
        root = root->left;
      } else {
        // 2 children
        remove_two(root, root->left);
      }
      delete temp;
    }
  }
}

// size - count of all words

unsigned BSTDictionary::size(const BSTDictionary::Node *root) {
  if (root == nullptr) {
    return 0;
  }
  return root->count + size(root->left) + size(root->right);
}

// clear dictionary

void BSTDictionary::destroy(BSTDictionary::Node *root) {
  if (root == nullptr) {
    return;
  }
  destroy(root->left);
  destroy(root->right);
  delete root;
}

// print dictionary

void BSTDictionary::print(const BSTDictionary::Node *root, std::ostream &os) {
  if (root == nullptr) {
    return;
  }
  print(root->left, os);
  os << (root->count) << ":\t" << root->word << std::endl;
  print(root->right, os);
}

std::ostream &operator<<(std::ostream &os, BSTDictionary const &dict) {
  BSTDictionary::print(dict.root, os);
  return os;
}
