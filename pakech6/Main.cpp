#include <iostream>
#include "Dictionary.h"
#include "AVLDictionary.h"
#include "BSTDictionary.h"

int main() {
    AVLDictionary *tree = new AVLDictionary();
    std::cout << "creating a tree////////////////////////" << std::endl;
    tree->insert("Ghost");
    tree->insert("Anya");
    tree->insert("Anya");
    tree->insert("Anya");
    tree->insert("Anya");
    tree->insert("Anya");
    tree->insert("Kenny");
    tree->insert("Hello");
    tree->insert("James");
    tree->insert("Easy");
    tree->insert("Claw");
    tree->insert("Denis");
    tree->insert("Bottle");
    tree->insert("Fedya");
    tree->insert("James");
    tree->insert("Claw");
    tree->insert("James");
    tree->insert("Easy");
    tree->insert("Claw");
    tree->insert("Idea");
    std::cout << *tree << std::endl;
    tree->print();
    std::cout << "find countwords, and some count of words////////////////////////" << std::endl;
    std::cout << tree->size() << std::endl;
    std::cout << tree->count("Anya") << std::endl;
    std::cout << tree->count("Ghost") << std::endl;
    std::cout << tree->count("Timmy") << std::endl;
    std::cout << "delete some words and print////////////////////////" << std::endl;
    tree->remove("Claw");
    tree->remove("Bottle");
    tree->remove("Anya");
    tree->remove("Ghost");
    std::cout << *tree << std::endl;
    tree->print();
    std::cout << tree->size() << std::endl;
    std::cout << tree->count("Anya") << std::endl;
    std::cout << tree->count("Ghost") << std::endl;
    std::cout << tree->count("Timmy") << std::endl;
    std::cout << "add word and print////////////////////////" << std::endl;
    tree->insert("Magic");
    std::cout << *tree << std::endl;
    tree->print();
    std::cout << "add numbers and print////////////////////////" << std::endl;
    tree->insert("40");
    tree->insert("60");
    tree->insert("30");
    tree->insert("50");
    tree->insert("70");
    tree->insert("20");//1
    tree->insert("20");//2
    tree->insert("20");//3
    tree->insert("35");
    tree->remove("20");//2
    std::cout << tree->count("20");
    std::cout << *tree;
    tree->print();

    delete tree;
    return 0;
}