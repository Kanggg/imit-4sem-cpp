#pragma once
#include <stdexcept>


class RingBuf {
private:
    int *data;
    int cap; // Capacity
    int beginPos;
    int m_size;

public:
    RingBuf(int size): cap(size+1), beginPos(0), m_size(0) {
        data = new int[size+1];
    }

    void pushBack(int elem) {
        if (m_size == cap - 1) {
            throw std::out_of_range("Out of range");
        }
        data[(beginPos + m_size) % cap] = elem;
        m_size++;
    }

    int pop() {
        int res = front();
        m_size--;
        beginPos++;
        if (beginPos == cap) {
            beginPos = 0;
        }
        return res;
    }

    int front() const {
        if (isEmpty()) {
            throw std::out_of_range("Out of range");
        }
        return data[beginPos];
    }

    inline int size() const {
        return m_size;
    }

    void clear() {
        beginPos = 0;
        m_size = 0;
    }

    inline bool isEmpty() const {
        return m_size == 0;
    }

    class Iterator {
    private:
        const RingBuf *ringBuf;
        int pos;

    public:
        explicit Iterator(const RingBuf *rb): pos(ringBuf->beginPos), ringBuf(rb) {
        }

        void start() {
            pos = ringBuf->beginPos;
        }

        void next() {
            if (finish()) {
                throw std::out_of_range("Out of bound");
            }
            pos++;
            if (pos == ringBuf->cap) {
                pos = 0;
            }
        }

        bool finish() {
            return ringBuf->isEmpty() ||
                    pos == (ringBuf->beginPos + ringBuf->m_size) % ringBuf->cap;
        }

        int getValue() {
            if (finish()) {
                throw std::out_of_range("Out of bound");
            }
            return ringBuf->data[pos];
        }
    };
};
