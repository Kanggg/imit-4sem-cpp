#pragma once

#include <stdexcept>
#include <algorithm> // copy and fill

class DynArr {
private:
    static const int DEFAULT_SIZE = 32;
    static const int AUTO_RESERVE_SIZE = 32;
    static const int DEFAULT_FILL_VALUE = 0;

    int *data;
    int _size;
    int _cap; // Capacity

    void check_cap(int cap) const {
        if (cap <= 0) {
            throw std::invalid_argument("Capacity should be positive");
        }
    }

    void check_pos(int pos) const {
        if (pos < 0 || pos >= _size) {
            throw std::out_of_range("Index out of range");
        }
    }

public:
    explicit DynArr(
            int init_size = DEFAULT_SIZE,
            int fill_value = DEFAULT_FILL_VALUE,
            int cap = 0
    ) :_size(init_size), _cap(cap) {
        if (_cap == 0) {
            _cap = _size + AUTO_RESERVE_SIZE;
        } else if (_cap < _size) {
            throw std::invalid_argument("Capacity can not be less than size");
        }

        check_cap(_cap);
        data = new int[_cap];
        std::fill(data, data + _size, fill_value);
    }

    // copy
    DynArr(const DynArr &src) : _size(src._size), _cap(src._cap) {
        data = new int[_cap];
        std::copy(src.data, src.data + _size, data);
    }

    // move
    DynArr(DynArr &&src) noexcept : data(src.data), _size(src._size), _cap(src._cap) {
        src.data = nullptr;
    }

    DynArr &operator=(const DynArr &src) {
        if (this == &src) {
            return *this;
        }
//        DynArr copy(src);
//        std::swap(data, copy.data);
//        _size = src._size;
//        _cap = src._cap;
        delete[] data;
        _cap = src._cap;
        _size = src._size;
        data = new int[src._cap];
        std::copy(src.data, src.data + _size, data);

        return *this;
    }

    DynArr &operator=(DynArr &&src) noexcept {
        delete[] data;
        _size = src._size;
        _cap = src._cap;
        data = src.data;
        src.data = nullptr;
        return *this;
    }

    ~DynArr() {
        delete[] data;
    }

    // get
    int operator[](int pos) const {
        check_pos(pos);
        return data[pos];
    }

    // set
    int &operator[](int pos) {
        check_pos(pos);
        return data[pos];
    }

    bool operator==(const DynArr &other) const {
        if (_size != other._size) {
            throw std::logic_error("Cant compare vectors with different sizes");
        }

        for (int i = 0; i < _size; i++) {
            if (data[i] != other.data[i]) {
                return false;
            }
        }

        return true;
    }

    bool operator!=(const DynArr &other) const {
        return !operator==(other);
    }

    bool operator<(const DynArr &other) const {
        int len = std::min(_size, other._size);

        for (int i = 0; i < len; i++) {
            if (data[i] < other.data[i]) {
                return true;
            } else if (data[i] > other.data[i]) {
                return false;
            }
        }

        return _size < other._size;
    }

    bool operator>(const DynArr &other) const {
        return other < *this;
    }

    bool operator<=(const DynArr &other) const {
        return !operator>(other);
    }

    bool operator>=(const DynArr &other) const {
        return !operator<(other);
    }

    // concat
    DynArr operator+(const DynArr &other) const {
        DynArr vec(_size + other._size);
        std::copy(data, data + _size, vec.data);
        std::copy(other.data, other.data + other._size, vec.data + _size);
        return vec;
    }

    inline int size() const {
        return _size;
    }

    inline int capacity() const {
        return _cap;
    }

    void push_back(int value) {
        resize(_size + 1);
        data[_size - 1] = value;
    }

    int pop_back() {
        if (_size <= 0) {
            throw std::logic_error("Cant pop from empty vector");
        }

        resize(_size - 1);
        return data[_size];
    }

    void resize(int new_size, int fill_value = DEFAULT_FILL_VALUE);
    void reserve(int n);

    friend std::ostream &operator<<(std::ostream &os, DynArr const &vec);
    friend std::istream &operator>>(std::istream &os, DynArr &vec);
};
