#include <iostream>
#include <string>

#include "AVLDictionary.h"

// copy

AVLDictionary::Node *AVLDictionary::copy(const AVLDictionary::Node *src) {
  if (src == nullptr) {
    return nullptr;
  }
  return new Node(
      src->word,
      src->count,
      copy(src->left),
      copy(src->right),
      src->height
  );
}

AVLDictionary::AVLDictionary(const AVLDictionary &src) : root(nullptr) {
  root = copy(src.root);
}

AVLDictionary::AVLDictionary(AVLDictionary &&src) noexcept: root(src.root) {
  src.root = nullptr;
}

AVLDictionary &AVLDictionary::operator=(const AVLDictionary &src) {
  if (&src != this) {
    clear();
    root = copy(src.root);
  }
  return *this;
}

AVLDictionary &AVLDictionary::operator=(AVLDictionary &&src) noexcept {
  clear();
  root = src.root;
  src.root = nullptr;
  return *this;
}

// count

unsigned AVLDictionary::count(const std::string &word) const {
  const Node *cur = root;
  int comp;

  while (cur != nullptr && (comp = compare_words(word, cur->word)) != 0) {
    cur = comp < 0 ? cur->left : cur->right;
  }

  return cur == nullptr ? 0 : cur->count;
}

// rotations

void AVLDictionary::rotate_R(AVLDictionary::Node *&root) {
  Node *temp = root->left;

  root->left = temp->right;
  temp->right = root;

  root->height = std::max(height(root->left), height(root->right)) + 1;
  temp->height = std::max(height(temp->left), height(temp->right)) + 1;

  root = temp;
}

void AVLDictionary::rotate_L(AVLDictionary::Node *&root) {
  Node *temp = root->right;

  root->right = temp->left;
  temp->left = root;

  root->height = std::max(height(root->left), height(root->right)) + 1;
  temp->height = std::max(height(temp->left), height(temp->right)) + 1;

  root = temp;
}

// insert element

void AVLDictionary::insert(
    AVLDictionary::Node *&root,
    const std::string &word
) {
  if (root == nullptr) {
    root = new AVLDictionary::Node(word);
    return;
  }

  int comp = compare_words(word, root->word);
  if (comp < 0) {
    insert(root->left, word);
  } else if (comp > 0) {
    insert(root->right, word);
  } else {
    // increment
    root->count++;
    return;
  }

  root->height = std::max(height(root->left), height(root->right)) + 1;
  auto bal = balance(root);

  if (bal > 1) {
    if (balance(root->left) > 0) {
      rotate_R(root);
    } else {
      rotate_L(root->left);
      rotate_R(root);
    }
  } else if (bal < -1) {
    if (balance(root->right) > 0) {
      rotate_R(root->right);
      rotate_L(root);
    } else {
      rotate_L(root);
    }
  }
}

// remove element

AVLDictionary::Node *AVLDictionary::get_left_leaf(AVLDictionary::Node *root) {
  Node* cur = root;

  while (cur->left != nullptr) {
    cur = cur->left;
  }

  return cur;
}

void AVLDictionary::remove(
    AVLDictionary::Node *&root,
    const std::string &word
) {
  if (root == nullptr) {
    return;
  }

  int comp = compare_words(word, root->word);
  if (comp < 0) {
    remove(root->left, word);
  } else if (comp > 0) {
    remove(root->right, word);
  } else {
    if (root->count > 1) {
      // decrement
      root->count--;
      return;
    }

    if (root->left == nullptr || root->right == nullptr) {
      // one or no child
      Node *temp = root->left == nullptr ? root->right : root->left;

      if (temp == nullptr) {
        temp = root;
        root = nullptr;
      } else {
        *root = *temp;
      }

      delete temp;
    } else {
      // two children
      Node *temp = get_left_leaf(root->right);
      root->word = temp->word;
      root->count = temp->count;

      remove(root->right, temp->word);
    }
  }

  if (root == nullptr) {
    return;
  }

  root->height = std::max(height(root->left), height(root->right)) + 1;
  auto bal = balance(root);

  if (bal > 1) {
    if (balance(root->left) > 0) {
      rotate_R(root);
    } else {
      rotate_L(root->left);
      rotate_R(root);
    }
  } else if (bal < -1) {
    if (balance(root->right) > 0) {
      rotate_R(root->right);
      rotate_L(root);
    } else {
      rotate_L(root);
    }
  }
}

// size - count of all words

unsigned AVLDictionary::size(const AVLDictionary::Node *root) {
  if (root == nullptr) {
    return 0;
  }
  return root->count + size(root->left) + size(root->right);
}

// clear dictionary

void AVLDictionary::destroy(AVLDictionary::Node *root) {
  if (root == nullptr) {
    return;
  }
  destroy(root->left);
  destroy(root->right);
  delete root;
}

// print dictionary

void AVLDictionary::print(const AVLDictionary::Node *root, std::ostream &os) {
  if (root == nullptr) {
    return;
  }
  print(root->left, os);
  os << (root->count) << ":\t" << root->word << std::endl;
  print(root->right, os);
}

std::ostream &operator<<(std::ostream &os, AVLDictionary const &dict) {
  AVLDictionary::print(dict.root, os);
  return os;
}

// print

void AVLDictionary::printTree(
        const AVLDictionary::Node *root,
        std::ostream &os, int level
) {
    if (root == nullptr) {
        return;
    }

    for (int i = 0; i < level; i++) {
        os << "  ";
    }
    os << root->word << std::endl;
    printTree(root->left, os, level + 1);
    printTree(root->right, os, level + 1);
}
