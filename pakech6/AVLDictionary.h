#ifndef AVLDICTIONARY_H
#define AVLDICTIONARY_H

#include <string>
#include "Dictionary.h"
#include "BSTDictionary.h"

class AVLDictionary : public Dictionary {
 private:

  struct Node {
    std::string word;
    unsigned count;
    Node *left, *right;
    signed char height;

    Node(std::string word, unsigned count, Node* left, Node* right, signed char height = 1) :
        word(std::move(word)), count(count), left(left), right(right), height(height) {}
    explicit Node(const std::string &word) : Node(word, 1, nullptr, nullptr) {}
  };

  Node *root;

 public:
  AVLDictionary() : root(nullptr) {}
  AVLDictionary(const AVLDictionary &src);
  AVLDictionary(AVLDictionary &&src) noexcept;
  ~AVLDictionary() override {
    clear();
  }

  AVLDictionary &operator=(const AVLDictionary &src);
  AVLDictionary &operator=(AVLDictionary &&src) noexcept;

  unsigned count(const std::string &word) const override;

  inline void insert(const std::string &word) override {
    insert(root, word);
  }

  inline void print() {
      printTree(root, std::cout, 0);
  }

  inline void remove(const std::string &word) override {
    remove(root, word);
  }

  inline unsigned size() const override {
    return size(root);
  }

  inline void clear() override {
    destroy(root);
    root = nullptr;
  }

  inline bool empty() override {
    return root == nullptr;
  }

  friend std::ostream &operator<<(std::ostream &os, AVLDictionary const &dict);

 private:
  static Node *copy(const Node *src);
  static void destroy(Node *root);
  static unsigned size(const Node *root);
  static void print(const Node *root, std::ostream &os);

  static void rotate_R(Node *&root);
  static void rotate_L(Node *&root);
  static Node* get_left_leaf(Node* root);

  static void insert(Node *&root, const std::string &word);
  static void remove(Node *&root, const std::string &word);
  static void printTree(
                const AVLDictionary::Node *root,
                std::ostream &os, int level
             );

  inline static signed char height(const Node *node) {
    return node == nullptr ? 0 : node->height;
  }

  inline static signed char balance(const Node *node) {
    return node == nullptr ? 0 : height(node->left) - height(node->right);
  }

  inline static int compare_words(const std::string &a, const std::string &b) {
    return a.compare(b);
  }
};

#endif
