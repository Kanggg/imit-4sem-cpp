#include "Container.h"
#include "Box.h"
#include "ContainerOverflowException.h"

using namespace std;
using namespace Garage;

int Container::boxCnt() const {
	return boxes.size();
}

double Container::sumWeight() const {
	double sum = 0.0;
	for (const Box& b: boxes){
		sum += b.getWeight();
	}
	return sum;
}

double Container::sumValue() const {
	double sum = 0.0;
	for (const Box& b: boxes){
		sum += b.getValue();
	}
	return sum;
}

const Box &Container::getBox(int index) const {
	return boxes.at(index);
}

void Container::showContainer() const {
	cout << (*this) << endl;
	for (const Box& b: boxes){
		cout << b << endl;
	}
}

int Container::push(Box box)  {
	if (box.getWeight() + sumWeight() > maxWeight) {
		throw ContainerOverflowException();
	}
	boxes.push_back(box);
	return boxes.size()-1;
}

void Container::pushInd(Box box, int index) {
	if (box.getWeight() + sumWeight() > maxWeight) {
		throw ContainerOverflowException();
	}
	boxes.insert(boxes.begin() + index, box);
}

void Container::delInd(int index) {
	boxes.erase(boxes.begin() + index);
}

/////////////////////////// ������� � �������

unsigned int Container::getLength() const {
	return length;
}

void Container::setLength(unsigned int length) {
	Container::length = length;
}

unsigned int Container::getWidth() const {
	return width;
}

void Container::setWidth(unsigned int width) {
	Container::width = width;
}

unsigned int Container::getHeight() const {
	return height;
}

void Container::setHeight(unsigned int height) {
	Container::height = height;
}

double Container::getMaxWeight() const {
	return maxWeight;
}

void Container::setMaxWeight(double maxWeight) {
	Container::maxWeight = maxWeight;
}
