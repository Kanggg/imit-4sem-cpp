#include "Box.h"
#include <algorithm>
using namespace std;
using namespace Garage;

int Box::sumVal(Box arr[], int arrSize) {
	int sum = 0;

	for (int i = 0; i < arrSize; i++) {
		sum += arr[i].getValue();
	}
	return sum;
}

bool Box::checkSize(Box arr[], int arrSize, int maxLength, int maxWidth, int maxHeight) {
	int sumLength;
	int sumWidth;
	int sumHeight;
	bool ret = true;

	for (int i = 0; i < arrSize; i++) {
		sumLength += arr[i].getLength();
		sumWidth += arr[i].getWidth();
		sumHeight += arr[i].getHeight();
	}

	if (sumLength > maxLength || sumWidth > maxWidth || sumHeight > maxHeight) {
		ret = false;
	}
	return ret;
}

double Box::maxWeightNoMoreVolume(Box arr[], int arrSize, int maxV) {
	double maxWeight = 0.0;
	for (int i = 0; i < arrSize; i++) {
		if (arr[i].getHeight() * arr[i].getWidth() * arr[i].getLength() <= maxV) {
			if (arr[i].getWeight() > maxWeight)
				maxWeight = arr[i].getWeight();
		}
	}
	return maxWeight;
}

bool Box::canStack(Box arr[], int arrSize) {
  Box* arrCopy = new Box[arrSize];

  copy(arr, arr + arrSize, arrCopy);
  sort(arrCopy, arrCopy + arrSize, [](Box &a, Box &b) {
    return a.length > b.length;
  });

  for (int i = 1; i < arrSize; i++) {
    if (arrCopy[i - 1].length == arrCopy[i].length ||
        arrCopy[i - 1].height <= arrCopy[i].height ||
        arrCopy[i - 1].width <= arrCopy[i].width) {
      delete[] arrCopy;
      return false;
    }
  }

  delete[] arrCopy;
  return true;
}

/////////////////////////////////////// ������� � �������

unsigned int Box::getLength() const {
  return length;
}

void Box::setLength(unsigned int length) {
  Box::length = length;
}

unsigned int Box::getWidth() const {
  return width;
}

void Box::setWidth(unsigned int width) {
  Box::width = width;
}

unsigned int Box::getHeight() const {
  return height;
}

void Box::setHeight(unsigned int height) {
  Box::height = height;
}

double Box::getWeight() const {
  return weight;
}

void Box::setWeight(double weight) {
  Box::weight = weight;
}

int Box::getValue() const {
  return value;
}

void Box::setValue(int value) {
  Box::value = value;
}
