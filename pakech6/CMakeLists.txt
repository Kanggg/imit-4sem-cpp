cmake_minimum_required(VERSION 3.13)
project(pakech6)

set(CMAKE_CXX_STANDARD 11)

include_directories(.)
add_executable(pakech6 Main.cpp Dictionary.h AVLDictionary.h AVLDictionary.cpp BSTDictionary.h BSTDictionary.cpp)