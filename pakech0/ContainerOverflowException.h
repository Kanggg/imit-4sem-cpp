#pragma once
class ContainerOverflowException : public std::exception {
 public:
  const char *what() const noexcept override {
    return "Container weight limit reached";
  }
};
