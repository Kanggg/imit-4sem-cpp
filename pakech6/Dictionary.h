#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>

class Dictionary {
 public:
  virtual ~Dictionary() = default;

  virtual unsigned count(const std::string &word) const = 0;
  virtual void insert(const std::string &word) = 0;
  virtual void remove(const std::string &word) = 0;
  virtual unsigned size() const = 0;
  virtual void clear() = 0;
  virtual bool empty() = 0;
};

#endif
