cmake_minimum_required(VERSION 3.14)
project(pakech1)

set(CMAKE_CXX_STANDARD 11)

add_executable(pakech1 main.cpp DynArr.cpp DynArr.h)