#include <iostream>
#include <sstream>
#include "DynArr.h"

int main(int argc, char** argv) {
    DynArr arr(2, 1, 15);
    std::cout << arr.size() << " " << arr.capacity() << std::endl;
    arr[0] = 0;
    DynArr copy(arr);
    arr.resize(0);
    arr.resize(5);
    std::cout << copy.size() << " " << copy.capacity() << " " << copy[0] << " " << copy[1] << std::endl; // must be 2 15 0 1

    // move constructor
    std::cout << "move constructor" << std::endl;
    DynArr move = std::move(arr);
    std::cout << " " << move << std::endl;

    // apply operator
    std::cout << "apply operator" << std::endl;
    arr = move;
    std::cout << arr.size() << " " << arr.capacity() << " " << arr[0] << " " << arr[1] << std::endl; // must be 5 15 0 0
    move[0] = 100;
    std::cout << arr << std::endl; // must be 0
    std::cout << move << std::endl; // must be 100

    // move operator
    std::cout << "move operator" << std::endl;
    arr = DynArr(2, 1, 15);
    std::cout << arr.size() << " " << arr.capacity() << " " << arr[0] << " " << arr[1] << std::endl; // must be 2 15 1 1

    // equals operators
    std::cout << "equals operators" << std::endl;
    DynArr a(5), b(a);
    std::cout << (a == b) << " " << (a != b)  << std::endl; // must be 1 0
    a[2] = 1;
    std::cout << (a == b) << " " << (a != b) << std::endl; // must be 0 1

    // compare operators
    std::cout << "compare operators" << std::endl;
    DynArr c(5), d(5);
    std::cout << (c < d) << " " << (c > d) << std::endl; // must be 0 0
    std::cout << (c <= d) << " " << (c >= d) << std::endl; // must be 1 1

    a = DynArr(10, 1);
    b = DynArr(5, 1);
    std::cout << (a < b) << " " << (a > b) << std::endl; // must be 0 1
    std::cout << (a <= b) << " " << (a >= b) << std::endl; // must be 0 1

    // concat operator
    std::cout << "concat operator" << std::endl;
    DynArr expected(5, 3);
    expected[0] = 2;
    expected[1] = 2;
    std::cout << (expected == DynArr(2, 2) + DynArr(3, 3)) << std::endl; // must be 1

    // input operations
    std::cout << "input operations" << std::endl;
    std::ostringstream oss;
    DynArr vec(5, 1, 10);
    oss << vec;
    std::cout << oss.str() << std::endl; // must be 5 1 1 1 1 1

    // read
    std::cout << "read" << std::endl;
    std::istringstream iss("3 -1 0 1");
    iss >> vec;
    std::cout << vec.size() << " " << vec[0] << " " << vec[1] << " " << vec[2] << std::endl; // must be 3 -1 0 1

    // pushback
    std::cout << "pushback" << std::endl;
    DynArr vec1(0, 0, 1);
    vec1.push_back(1);
    std::cout << vec1 << std::endl; // must be 1

    //increase size
    std::cout << "increase size" << std::endl;
    DynArr vec2(0, 0, 1);
    vec2.push_back(2);
    std::cout << vec2.size() << " " << vec2.capacity() << std::endl; // must be 1 1

    // increase capacity
    std::cout << "increase capacity" << std::endl;
    DynArr vec3(1, 0, 1);
    vec3.push_back(3);
    std::cout << vec3.size() << " " << vec3.capacity() << std::endl; // must be 2 34

    // pop back
    std::cout << "pop back" << std::endl;
    vec1 = DynArr(2);
    vec1[0] = 5;
    vec1[1] = 10;
    std::cout << vec1.pop_back() << std::endl; // must be 10

    //decrease size
    std::cout << "decrease size" << std::endl;
    vec2 = DynArr(1);
    vec2.pop_back();
    std::cout << vec2.size() << " " << vec2.capacity() << std::endl; // must be 0 33

    // resize
    std::cout << "resize" << std::endl;
    vec = DynArr(5, 0, 10);
    vec.resize(7);
    std::cout << vec.size() << " " << vec.capacity() << std::endl; // must be 7 10
    // fills with given elem
    vec.resize(10, 1);
    std::cout << vec[9] << std::endl; // must be 1
    // decrease size
    vec.resize(0);
    std::cout << vec.size() << " " << vec.capacity() << std::endl; // must be 0 10
    // increase size
    vec.resize(15);
    std::cout << vec.capacity() << std::endl; // must be 47

    // reserve
    std::cout << "reserve" << std::endl;
    vec = DynArr(10, 0, 15);
    // decrease capacity
    vec.reserve(0);
    std::cout << vec.size() << " " << vec.capacity() << std::endl; // must be 10 10
    // increase capacity
    vec.reserve(10);
    std::cout << vec.size() << " " << vec.capacity() << std::endl; // must be 10 20


	return 0;
}
