#include <iostream> // << and >> operators for primitive types
#include <stdexcept>
#include <algorithm> // fill and copy

#include "DynArr.h"

void DynArr::resize(int new_size, int fill_value) {
    if (new_size > _cap) {
        reserve(new_size - _size + AUTO_RESERVE_SIZE);
    }

    if (new_size > _size) {
        std::fill(data + _size, data + new_size, fill_value);
    }
    _size = new_size;
}

void DynArr::reserve(int n) {
    if (n == _cap - _size) {
        return;
    }
    check_cap(_size + n);

    auto *new_data = new int[_size + n];
    std::copy(data, data + _size, new_data);
    delete[] data;
    data = new_data;
    _cap = _size + n;
}

// write
std::ostream &operator<<(std::ostream &os, const DynArr &vec) {
    os << vec._size;
    for (int i = 0; i < vec._size; i++) {
        os << ' ' << vec.data[i];
    }

    return os;
}

// read
std::istream &operator>>(std::istream &is, DynArr &vec) {
    int new_size;
    is >> new_size;
    DynArr new_vec(new_size);

    for (int i = 0; i < new_size && !is.fail(); i++) {
        is >> new_vec.data[i];
    }

    if (is.fail()) {
        throw std::runtime_error("Wrong input");
    }

    vec = new_vec;
    return is;
}
