#pragma once
#include <iostream>
using namespace std;

namespace Garage {

	class Box {
		private:
			int length;
			int width;
			int height;
			double weight;
			int value;

		public:
			Box(int length, int width, int height, double weight, int value)
				: length(length),
				  width(width),
				  height(height),
				  weight(weight),
				  value(value) {
			}
			Box()
				: length(0),
				  width(0),
				  height(0),
				  weight(0),
				  value(0) {
			}
			bool operator == (const Box &other) const {
				return length == other.length && width == other.width && height == other.height && weight == other.weight && value == other.value;
			}
			friend ostream &operator << (ostream &s, const Box& v) {
				s << v.length << " " << v.width << " " << v.height << " " << v.weight << " " << v.value;
				return s;
			}
			friend istream &operator >> (istream &s, Box& v) {
				s >> v.length >> v.width >> v.height >> v.weight >> v.value;
				return s;
			}
			unsigned int getLength() const;
			void setLength(unsigned int length);
			unsigned int getWidth() const;
			void setWidth(unsigned int width);
			unsigned int getHeight() const;
			void setHeight(unsigned int height);
			double getWeight() const;
			void setWeight(double weight);
			int getValue() const;
			void setValue(int value);
			
			static int sumVal(Box arr[], int arrSize);
			static bool checkSize(Box arr[], int arrSize, int maxLength, int maxWidth, int maxHeight);
			static double maxWeightNoMoreVolume(Box arr[], int arrSize, int maxV);
			static bool canStack(Box arr[], int arrSize);
			
	};
}
