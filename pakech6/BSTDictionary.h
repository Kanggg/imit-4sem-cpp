#ifndef BSTDICTIONARY_H
#define BSTDICTIONARY_H

#include <string>
#include "Dictionary.h"

class BSTDictionary : public Dictionary {
 private:
  struct Node {
    std::string word;
    unsigned count;
    Node *left, *right;

    Node(std::string word, unsigned count, Node* left, Node* right) :
        word(std::move(word)), count(count), left(left), right(right) {}
    explicit Node(const std::string &word) : Node(word, 1, nullptr, nullptr) {}
  };

  Node *root;

 public:
  BSTDictionary() : root(nullptr) {}
  BSTDictionary(const BSTDictionary &src);
  BSTDictionary(BSTDictionary &&src) noexcept;
  ~BSTDictionary() override {
    clear();
  }

  BSTDictionary &operator=(const BSTDictionary &src);
  BSTDictionary &operator=(BSTDictionary &&src) noexcept;

  inline unsigned count(const std::string &word) const override {
    return find(root, word);
  }

  inline void insert(const std::string &word) override {
    insert(root, word);
  }

  inline void remove(const std::string &word) override {
    remove(root, word);
  }

  inline unsigned size() const override {
    return size(root);
  }

  inline void clear() override {
    destroy(root);
    root = nullptr;
  }

  inline bool empty() override {
    return root == nullptr;
  }

  friend std::ostream &operator<<(std::ostream &os, BSTDictionary const &dict);

 private:
  static Node *copy(const Node *src);
  static void destroy(Node *root);
  static unsigned find(const Node *root, const std::string &word);
  static void insert(Node *&root, const std::string &word);
  static void remove_two(Node *&root, Node *&left_child);
  static void remove(Node *&root, const std::string &word);
  static unsigned size(const Node *root);
  static void print(const Node *root, std::ostream &os);

  inline static int compare_words(const std::string &a, const std::string &b) {
    return a.compare(b);
  }
};

#endif
