#pragma once
#include <iostream>
#include <vector>
#include "Box.h"

using namespace std;

namespace Garage {

	class Container {
		private:
			vector<Box> boxes;
			int length;
			int width;
			int height;
			double maxWeight;
		public:
			Container(int length, int width, int height, double maxWeight)
				: length(length),
				  width(width),
				  height(height),
				  maxWeight(maxWeight) {}

			friend ostream &operator << (ostream &s, const Container& v) {
				s << v.length << " " << v.width << " " << v.height << " " << v.maxWeight;
				return s;
			}
			friend istream &operator >> (istream &s, Container& v) {
				s >> v.length >> v.width >> v.height >> v.maxWeight;
				return s;
			}
			const Box& operator [] (int index) const {
				return getBox(index);
			}

			int boxCnt() const;
			double sumWeight() const;
			double sumValue() const;
			const Box &getBox(int index) const;
			void showContainer() const;
			int push(Box box);
			void delInd(int index);
			void pushInd(Box box, int index);

			unsigned int getLength() const;
			void setLength(unsigned int length);
			unsigned int getWidth() const;
			void setWidth(unsigned int width);
			unsigned int getHeight() const;
			void setHeight(unsigned int height);
			double getMaxWeight() const;
			void setMaxWeight(double maxWeight);
	};
}
