#include "LinkedHashTable.h"
#include <iostream>

using namespace std;

int main() {
    cout << "add elements to linked hash table and print using linkedHashTable" << endl;
    auto *oneHashTable = new LinkedHashTable();
    oneHashTable->addElem(1, 0);
    oneHashTable->addElem(2, 3);
    oneHashTable->addElem(3, 2);
    oneHashTable->addElem(4, 5);
    oneHashTable->addElem(15, 7);
    oneHashTable->addElem(5, 12);
    oneHashTable->printElem();
    Iterator iteratorOne(oneHashTable);
    cout << "print elems using iterator" << endl;
    while (iteratorOne.hasNext()) {
        cout << iteratorOne.seeElem() << endl;
        iteratorOne.next();
    }
    cout << iteratorOne.seeElem() << endl;
    cout << "deleting elem 2" << endl;
    oneHashTable->removeElem(2);
    oneHashTable->printElem();
    iteratorOne.start();
    while (iteratorOne.hasNext()) {
        cout << iteratorOne.seeElem() << endl;
        iteratorOne.next();
    }
    cout << iteratorOne.seeElem() << endl;
    cout << "other methods" << endl;
    cout << oneHashTable->getElem(0) << endl;
    cout << oneHashTable->getElem(5) << endl;
    cout << "isEmpty: " << oneHashTable->isEmpty() << endl;
    cout << "doEmpty." << endl;
    oneHashTable->doEmpty();
    cout << "isEmpty: " << oneHashTable->isEmpty() << endl;
    return 0;
}

