#include <iostream>
#include "RingBuf.h"

using namespace std;

int main(int argc, char** argv) {
    RingBuf rb(27);
    cout << rb.size() << endl;
    for (int i = 0; i < 20; i++) {
        rb.pushBack(i);
    }
    cout << rb.size() << " " << rb.isEmpty() << " " << rb.front() << endl;

    for (int i = 0; i < 10; i++) {
        cout << rb.pop() << " ";
    }
    cout << endl;
    cout << rb.size() << " " << rb.front() << endl;

    for (int i = 0; i < 17; i++) {
        rb.pushBack(i);
    }
    cout << rb.size() << " " << rb.front() << endl;

//    for (int i = 0; i < 27; i++) {
//        cout << rb.pop() << " ";
//    }
//    cout << endl;
//
//    cout << rb.size() << " " << endl;

    cout << "Iterator" << endl;

    RingBuf::Iterator it(&rb);

    while (!it.finish()) {
        cout << it.getValue() << " ";
        it.next();
    }

    return 0;
}